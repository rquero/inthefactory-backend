<?php


return [

    // The prefix for routes
    'prefix' => 'graphql',

    // The routes to make GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Route
    //
    // Example:
    //
    // Same route for both query and mutation
    //
    // 'routes' => 'path/to/query/{graphql_schema?}',
    //
    // or define each routes
    //
    // 'routes' => [
    //     'query' => 'query/{graphql_schema?}',
    //     'mutation' => 'mutation/{graphql_schema?}',
    //     'mutation' => 'graphiql'
    // ]
    //
    // you can also disable routes by setting routes to null
    //
    // 'routes' => null,
    //
    'routes' => '{graphql_schema?}',

    // The controller to use in GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Controller and method
    //
    // Example:
    //
    // 'controllers' => [
    //     'query' => '\Folklore\GraphQL\GraphQLController@query',
    //     'mutation' => '\Folklore\GraphQL\GraphQLController@mutation'
    // ]
    //
    'controllers' => \Folklore\GraphQL\GraphQLController::class.'@query',

    // The name of the input that contain variables when you query the endpoint.
    // Some library use "variables", you can change it here. "params" will stay
    // the default for now but will be changed to "variables" in the next major
    // release.
    'variables_input_name' => 'variables',

    // Any middleware for the graphql route group
    'middleware' => [],

     // Any headers that will be added to the response returned by the default controller
    'headers' => [],

    // Any json encoding options when returning a response from the default controller
    // See http://php.net/manual/function.json-encode.php for list of options
    'json_encoding_options' => 0,

    // Config for GraphiQL (https://github.com/graphql/graphiql).
    // To disable GraphiQL, set this to null.
    'graphiql' => [
        'routes' => '/graphiql/{graphql_schema?}',
        'middleware' => [],
        'controller' => \Folklore\GraphQL\GraphQLController::class.'@graphiql',
        'view' => 'graphql::graphiql'
    ],

    // The name of the default schema used when no argument is provided
    // to GraphQL::schema() or when the route is used without the graphql_schema
    // parameter.
    'schema' => 'default',

    // The schemas for query and/or mutation. It expects an array to provide
    // both the 'query' fields and the 'mutation' fields. You can also
    // provide directly an object GraphQL\Schema
    //
    // Example:
    //
    // 'schemas' => [
    //     'default' => new Schema($config)
    // ]
    //
    // or
    //
    // 'schemas' => [
    //     'default' => [
    //         'query' => [
    //              'users' => 'App\GraphQL\Query\UsersQuery'
    //          ],
    //          'mutation' => [
    //
    //          ]
    //     ]
    // ]
    //
    'schemas' => [
        'default' => [
            'query' => [

                'getCurrent' => App\GraphQL\Query\GetCurrentQuery::class,

                'userByID' => App\GraphQL\Query\UserQuery::class,
                'userPagination' => App\GraphQL\Query\UsersPaginationQuery::class,
                'users' => App\GraphQL\Query\UsersQuery::class,

                //'roleByID' => App\GraphQL\Query\RoleQuery::class,
                //'rolePagination' => App\GraphQL\Query\RolesPaginationQuery::class,
                //'roles' => App\GraphQL\Query\RolesQuery::class,

                'countrys' => App\GraphQL\Query\CountrysQuery::class,
                'countryStates' => App\GraphQL\Query\CountryStatesQuery::class,
                'states' => App\GraphQL\Query\StatesQuery::class,

                'questionByID' => App\GraphQL\Query\QuestionQuery::class,

                'categorys'    => App\GraphQL\Query\CategorysQuery::class,
                'categoryById' => App\GraphQL\Query\CategoryQuery::class,

                'subCategorys'    => App\GraphQL\Query\SubCategorysQuery::class,
                'subCategoryById' => App\GraphQL\Query\SubCategoryQuery::class,

                'customerProfile' => App\GraphQL\Query\CustomerProfileQuery::class,

                'providerProfile' => App\GraphQL\Query\ProviderProfileQuery::class,





        
            ],
            'mutation' => [

                'login' => App\GraphQL\Mutation\Auth\CreateTokenMutation::class,
                'refreshToken' => App\GraphQL\Mutation\Auth\RefreshTokenMutation::class,
                'logout' => App\GraphQL\Mutation\Auth\LogoutMutation::class,

            

               // 'createRole' => App\GraphQL\Mutation\Role\CreateRoleMutation::class,
               // 'deleteRole' => App\GraphQL\Mutation\Role\DeleteRoleMutation::class,
               // 'updateRole' => App\GraphQL\Mutation\Role\UpdateRoleMutation::class,
               // 'blockedRole' => App\GraphQL\Mutation\Role\BlockedRoleMutation::class,

                'updateCompanyProfile' => App\GraphQL\Mutation\User\UpdateCompanyProfileMutation::class,
                 
                'updateShippingAddress' => App\GraphQL\Mutation\User\UpdateShippingAddressMutation::class,
                'updateBillingAddress' => App\GraphQL\Mutation\User\UpdateBillingAddressMutation::class,

                'updatePreferences' => App\GraphQL\Mutation\User\UpdatePreferencesMutation::class,


                'registerProvider' => App\GraphQL\Mutation\User\RegisterProviderMutation::class,
                'registerCustomer' => App\GraphQL\Mutation\User\RegisterCustomerMutation::class,
               // 'deleteUser' => App\GraphQL\Mutation\User\DeleteUserMutation::class,
               // 'updateUser' => App\GraphQL\Mutation\User\UpdateUserMutation::class,
               //'updateEmailUser' => App\GraphQL\Mutation\User\UpdateEmailMutation::class,
                'resetPasswordUser' => App\GraphQL\Mutation\User\ResetPasswordMutation::class,
                'resetPasswordIdUser' => App\GraphQL\Mutation\User\ResetPasswordIdMutation::class,
               // 'blockedUser' => App\GraphQL\Mutation\User\BlockedUserMutation::class,
               // 'updateLastNames' => App\GraphQL\Mutation\User\UpdateLastNamesMutation::class,
               // 'updateNames' => App\GraphQL\Mutation\User\UpdateNamesMutation::class,
               // 'updatePhone' => App\GraphQL\Mutation\User\UpdatePhoneMutation::class,
               // 'updateFirstAndLastName' => App\GraphQL\Mutation\User\UpdateFirstAndLastNameMutation::class,

                'createAnswer' => App\GraphQL\Mutation\Answer\CreateAnswerMutation::class,
                'deleteAnswer' => App\GraphQL\Mutation\Answer\DeleteAnswerMutation::class,
                'updateAnswer' => App\GraphQL\Mutation\Answer\UpdateAnswerMutation::class,
                'blockedAnswer' => App\GraphQL\Mutation\Answer\BlockedAnswerMutation::class,

                'createQuestion' => App\GraphQL\Mutation\Question\CreateQuestionMutation::class,
                'deleteQuestion' => App\GraphQL\Mutation\Question\DeleteQuestionMutation::class,
                'updateQuestion' => App\GraphQL\Mutation\Question\UpdateQuestionMutation::class,
                'blockedQuestion' => App\GraphQL\Mutation\Question\BlockedQuestionMutation::class,
                'stateQuestion' => App\GraphQL\Mutation\Question\StateQuestionMutation::class,
                'canceledQuestion' => App\GraphQL\Mutation\Question\CanceledQuestionMutation::class,

            ]
        ],
        'secret' => [
            'query' => [
            ],
            'mutation' => [
            ],
        ]
    ],

    // The types available in the application. You can then access it from the
    // facade like this: GraphQL::type('user')
    //
    // Example:
    //
    // 'types' => [
    //     'user' => 'App\GraphQL\Type\UserType'
    // ]
    //
    // or whitout specifying a key (it will use the ->name property of your type)
    //
    // 'types' => [
    //     'App\GraphQL\Type\UserType'
    // ]
    //
    'types' => [

        
        \App\GraphQL\Type\AuthType::class,
        \App\GraphQL\Type\CountryType::class,
        \App\GraphQL\Type\StateType::class,
        \App\GraphQL\Type\StringType::class,

        \App\GraphQL\Type\RoleType::class,
        \App\GraphQL\Type\UserType::class,
        \App\GraphQL\Type\RoleMappingType::class,
        \App\GraphQL\Type\UserPaginationType::class,
        \App\GraphQL\Type\RolePaginationType::class,
        \App\GraphQL\Type\GetCurrentType::class,


  
        \App\GraphQL\Type\ProviderType::class,
        \App\GraphQL\Type\CustomerType::class,
        \App\GraphQL\Type\UserAuthType::class,
        \App\GraphQL\Type\CategoryType::class,
        \App\GraphQL\Type\SubCategoryType::class,
        

        \App\GraphQL\Type\ProductType::class,

        \App\GraphQL\Type\AnswerType::class,
        \App\GraphQL\Type\QuestionType::class,
        \App\GraphQL\Type\AddressType::class,

        \App\GraphQL\Type\UserAddressType::class,

        \App\GraphQL\Type\CustomerProfileType::class,
        \App\GraphQL\Type\ProviderProfileType::class,
        
        \App\GraphQL\Type\ValorationType::class,
        
        \App\GraphQL\Type\OrderType::class,





      //  \App\Models\AddressType::class,

    ],

    // This callable will received every Error objects for each errors GraphQL catch.
    // The method should return an array representing the error.
    //
    // Typically:
    // [
    //     'message' => '',
    //     'locations' => []
    // ]
    //
    'error_formatter' => [\Folklore\GraphQL\GraphQL::class, 'formatError'],

    // Options to limit the query complexity and depth. See the doc
    // @ https://github.com/webonyx/graphql-php#security
    // for details. Disabled by default.
    'security' => [
        'query_max_complexity' => null,
        'query_max_depth' => null,
        'disable_introspection' => false

    ],
    'params_key' => 'params'
];
