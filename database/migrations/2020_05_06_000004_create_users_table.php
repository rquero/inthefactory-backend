<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('phone');
            $table->string('image_profile');
            $table->string('image_banner');
            $table->string('full_name');
        
            $table->boolean('juridical');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('active');

            $table->rememberToken();
            $table->integer('state_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->timestamps();


            $table->foreign('state_id')->references('id')->on('states')
            ->onUpdate('cascade')
            ->onDetete('cascade');

            $table->foreign('role_id')->references('id')->on('roles')
            ->onUpdate('cascade')
            ->onDetete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
