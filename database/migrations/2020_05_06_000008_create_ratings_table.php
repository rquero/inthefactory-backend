<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            
            $table->increments('id');
            $table->enum('assessment1', [ 1,2,3,4,5])->nullable();
            $table->enum('assessment2', [ 1,2,3,4,5])->nullable();
            $table->enum('assessment3', [ 1,2,3,4,5])->nullable();
            $table->string('assessmentCustomer');
            $table->string('observations');
            $table->float('total', 8, 2);
            $table->enum('valuedBy', ['Provider','Customer'])->nullable();
           

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
