<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        DB::table('countrys')->insert([[
            'name' => "Argentina",
            'value' => "AR",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Bolivia",
            'value' => "BO",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Brasil",
            'value' => "BR",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Chile",
            'value' => "CL",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Colombia",
            'value' => "CO",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Costa Rica",
            'value' => "CR",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Ecuador",
            'value' => "EC",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "El Salvador",
            'value' => "SV",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Guatemala",
            'value' => "GT",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Honduras",
            'value' => "HN",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Nicaragua",
            'value' => "NI",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Panamá",
            'value' => "PA",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Paraguay",
            'value' => "PY",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Peru",
            'value' => "PE",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Puerto Rico",
            'value' => "PR",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Mexico",
            'value' => "MX",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "República Dominicana",
            'value' => "DO",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Uruguay",
            'value' => "UY",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Venezuela",
            'value' => "VE",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Cuba",
            'value' => "CU",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Curazao",
            'value' => "CW",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "España",
            'value' => "ES",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Haiti",
            'value' => "HT",
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]]);

    }
}