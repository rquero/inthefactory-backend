<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        DB::table('states')->insert([[
            'name' => "Bs.As. Costa Atlántica",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Bs.As. G.B.A. Norte",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Bs.As. G.B.A. Sur",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Buenos Aires Interior",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Capital Federal",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Catamarca",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Chaco",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Bs.As. G.B.A. Oeste",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Chubut",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Corrientes",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Córdoba",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Entre Ríos",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Formosa",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Jujuy",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Pampa",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "La Rioja",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Mendoza",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Misiones",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Neuquén",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Río Negro",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Salta",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "San Luis",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Santa Cruz",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Santa Fe",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Santiago del Estero",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Tierra del Fuego",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "San Juan",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Tucumán",
            'country_id' => 1,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Beni",
            'country_id' => 2,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Chuquisaca",
            'country_id' => 2,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Cochabamba",
            'country_id' => 2,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "La Paz",
            'country_id' => 2,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Oruro",
            'country_id' => 2,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Pando",
            'country_id' => 2,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Potosí",
            'country_id' => 2,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Santa Cruz",
            'country_id' => 2,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Tarija",
            'country_id' => 2,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Acre",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Alagoas",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Amapá",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Bahia",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Ceará",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Distrito Federal",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Espírito Santo",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Goiás",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Amazonas",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Maranhão",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ],[
            'name' => "Mato Grosso",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Mato Grosso do Sul",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Minas Gerais",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Paraná",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Paraíba",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Pará",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Pernambuco",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Rio Grande do Norte",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Rio Grande do Sul",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Rio de Janeiro",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Rondônia",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Piauí",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santa Catarina",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sergipe",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "São Paulo",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tocantins",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Roraima",
            'country_id' => 3,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Aisén",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Antofagasta",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Arica y Parinacota",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Atacama",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Biobío",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Coquimbo",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Inglaterra",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Araucanía",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Libertador B. O\'Higgins",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Los Lagos",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Los Ríos",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Magallanes",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Maule",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tarapacá",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Valparaíso",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Amazonas",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Antioquia",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Arauca",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "RM (Metropolitana)",
            'country_id' => 4,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Atlántico",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Bogotá D.C.",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Bolívar",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Boyaca",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Caldas",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Caqueta",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Casanare",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cauca",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Choco",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cundinamarca",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Córdoba",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guainía",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cesar",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guaviare",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Huila",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Magdalena",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Meta",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guajira",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Nariño",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Norte De Santander",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Putumayo",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Quindio",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Risaralda",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Andres y Providencia",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santander",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sucre",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tolima",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Valle Del Cauca",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Vaupes",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Vichada",
            'country_id' => 5,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cartago",
            'country_id' => 6,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guanacaste",
            'country_id' => 6,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Heredia",
            'country_id' => 6,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Limón",
            'country_id' => 6,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Puntarenas",
            'country_id' => 6,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Alajuela",
            'country_id' => 6,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San José",
            'country_id' => 6,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Azuay",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Bolívar",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Carchi",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cañar",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Chimborazo",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cotopaxi",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "El Oro",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Esmeraldas",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Galápagos",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guayas",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Imbabura",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Loja",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Manabí",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Morona Santiago",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Napo",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Orellana",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Pastaza",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Los Ríos",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Pichincha",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santa Elena",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santo Domingo de los Tsáchilas",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sucumbíos",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tungurahua",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Zamora Chinchipe",
            'country_id' => 7,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ahuachapán",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cabañas",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cuscatlán",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Libertad",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Paz",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Unión",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Morazán",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Chalatenango",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Miguel",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Salvador",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Vicente",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santa Ana",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sonsonate",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Usulután",
            'country_id' => 8,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Alta Verapaz",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Baja Verapaz",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Chiquimula",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "El Progreso",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Escuintla",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guatemala",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Chimaltenango",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Izabal",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Jalapa",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Jutiapa",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Petén",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Huehuetenango",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Quetzaltenango",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Quiché",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Retalhuleu",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sacatepéquez",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Marcos",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santa Rosa",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sololá",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Suchitepéquez",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Zacapa",
            'country_id' => 9,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Atlántida",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Choluteca",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Colón",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Comayagua",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Totonicapán",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Copán",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cortés",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "El Paraíso",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Francisco Morazán",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Gracias a Dios",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Intibucá",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Islas de la Bahía",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Paz",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Lempira",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ocotepeque",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Olancho",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santa Bárbara",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Valle",
            'country_id' => 10,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Aguascalientes",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Baja California",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Baja California Sur",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Campeche",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Chiapas",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Yoro",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Chihuahua",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Coahuila",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Colima",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Distrito Federal",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Durango",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Estado De México",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guanajuato",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guerrero",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Hidalgo",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Jalisco",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Michoacán",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Morelos",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Nayarit",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Oaxaca",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Puebla",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Querétaro",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Quintana Roo",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Luis Potosí",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Nuevo León",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sinaloa",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sonora",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tabasco",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tamaulipas",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tlaxcala",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Veracruz",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Yucatán",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Zacatecas",
            'country_id' => 16,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Atlántico Norte",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Atlántico Sur",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Boaco",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Carazo",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Chinandega",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Estelí",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Granada",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Jinotega",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "León",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Madriz",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Chontales",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Managua",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Masaya",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Matagalpa",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Nueva Segovia",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Rivas",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Río San Juan",
            'country_id' => 11,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Bocas del Toro",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Chiriquí",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Coclé",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Colón",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Darién",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Herrera",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Los Santos",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Panamá Centro",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Panamá Oeste",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Miguelito",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Veraguas",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Comarca Kuna Yala",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Panamá Este",
            'country_id' => 12,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Alto Paraguay",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Alto Paraná",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Amambay",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Asunción D.C.",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Boquerón",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Caaguazú",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Caazapá",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Canindeyú",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Central",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Concepción",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cordillera",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guairá",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Itapúa",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Paraguarí",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Presidente Hayes",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Pedro",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ñeembucú",
            'country_id' => 13,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Amazonas",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Misiones",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ancash",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Apurimac",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Arequipa",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ayacucho",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cajamarca",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Callao",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cusco",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Huanuco",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Huancavelica",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ica",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Junin",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Libertad",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Lambayeque",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Loreto",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Madre De Dios",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Moquegua",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Pasco",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Piura",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Lima",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Puno",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Martin",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tacna",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tumbes",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ucayali",
            'country_id' => 14,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Adjuntas",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Aguada",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Aguadilla",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Aibonito",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Añasco",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Arecibo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Arroyo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Aguas Buenas",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Barranquitas",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Bayamón",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cabo Rojo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Caguas",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Barceloneta",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Camuy",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Canóvanas",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Carolina",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cataño",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cayey",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ceiba",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ciales",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cidra",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Coamo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Comerío",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Corozal",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Culebra (Isla municipio)",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Dorado",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Florida",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guánica",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guayama",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guayanilla",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Fajardo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Gurabo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Hatillo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Hormigueros",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Humacao",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guaynabo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Isabela",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Jayuya",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Juana Díaz",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Juncos",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Lajas",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Lares",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Las Marías",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Las Piedras",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Luquillo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Manatí",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Maricao",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Maunabo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Mayagüez",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Loíza",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Moca",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Morovis",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Naguabo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Naranjito",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Orocovis",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Patillas",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Peñuelas",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ponce",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Rincón",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Río Grande",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sabana Grande",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Salinas",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Quebradillas",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Juan",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Lorenzo",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Sebastián",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santa Isabel",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Germán",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Toa Alta",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Toa Baja",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Trujillo Alto",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Utuado",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Vega Alta",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Vega Baja",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Vieques (Isla municipio)",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Villalba",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Yabucoa",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Yauco",
            'country_id' => 15,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Azua",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Baoruco",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Barahona",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Distrito Nacional",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Duarte",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "El Seibo",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Elías Piña",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Espaillat",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Dajabón",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Hato Mayor",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Independencia",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Altagracia",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Romana",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Vega",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "María Trinidad Sánchez",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Monseñor Nouel",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Monte Plata",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Montecristi",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Pedernales",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Peravia",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Puerto Plata",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Salcedo",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Cristóbal",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San José de Ocoa",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Juan",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San Pedro de Macorís",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santiago",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Samaná",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santiago Rodríguez",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santo Domingo",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sánchez Ramírez",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Valverde",
            'country_id' => 17,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Artigas",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Canelones",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cerro Largo",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Colonia",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Durazno",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Flores",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Florida",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Lavalleja",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Maldonado",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Paysandú",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Rivera",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Rocha",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Río Negro",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Salto",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Montevideo",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "San José",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Soriano",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tacuarembó",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Treinta y Tres",
            'country_id' => 18,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Amazonas",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Anzoátegui",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Apure",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Aragua",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Barinas",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Bolívar",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Carabobo",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cojedes",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Delta Amacuro",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Distrito Capital",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Falcón",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guárico",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Lara",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Miranda",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Depend. Federales",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Monagas",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Mérida",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Nueva Esparta",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Portuguesa",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sucre",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Trujillo",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Táchira",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Guiara",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Yaracuy",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Zulia",
            'country_id' => 19,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Provincias de Cuba",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Pinar del Río",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Provincia de Artemisa",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Provincia de Ciudad de La Habana",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Provincia de Mayabeque",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Matanzas",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cienfuegos",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Villa Clara",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sancti Spíritus",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ciego de Ávila",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Camagüey",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Las Tunas",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Granma",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Holguín",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santiago de Cuba",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guantánamo",
            'country_id' => 20,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Willemstad",
            'country_id' => 21,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sint-Michiel",
            'country_id' => 21,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tera Corá",
            'country_id' => 21,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Barber",
            'country_id' => 21,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Soto",
            'country_id' => 21,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Westpunt",
            'country_id' => 21,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sint Willibrordus",
            'country_id' => 21,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Lagun",
            'country_id' => 21,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Grote Berg",
            'country_id' => 21,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Álava",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Albacete",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Alicante",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Almería",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Asturias",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ávila",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Badajoz",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Islas Baleares",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Barcelona",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Burgos",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cáceres",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cádiz",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cantabria",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Castellón",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Ciudad Real",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Córdoba",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Cuenca",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Girona",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Granada",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guadalajara",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Guipúzcoa",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Huelva",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Huesca",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Jaén",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Rioja",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "La Coruña",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Las Palmas",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "León",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Lleida",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Lugo",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Madrid",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Málaga",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Murcia",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Navarra",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Orense",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Palencia",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Pontevedra",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Salamanca",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Segovia",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sevilla",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Soria",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Tarragona",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Santa Cruz de Tenerife",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Teruel",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Toledo",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Valencia",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Valladolid",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Vizcaya",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Zamora",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Zaragoza",
            'country_id' => 22,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Artibonito",
            'country_id' => 23,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Centro",
            'country_id' => 23,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "GrandAnse",
            'country_id' => 23,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Nippes",
            'country_id' => 23,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Norte",
            'country_id' => 23,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Noreste",
            'country_id' => 23,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Noroeste",
            'country_id' => 23,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Oeste",
            'country_id' => 23,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sudeste",
            'country_id' => 23,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ],[
            'name' => "Sur",
            'country_id' => 23,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]]);


    }
}

