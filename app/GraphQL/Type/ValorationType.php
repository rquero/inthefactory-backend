<?php
namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;

class ValorationType extends BaseType
{
   
    protected $attributes = [
        'name' => 'Valoration',
        'description' => 'Valoration type.'
    ];
    
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int())
            ],
            'facility' => [
                'type' => Type::int()
            ],
            'human' => [
                'type' => Type::int()
            ],
            'service' => [
                'type' => Type::int()
            ],
            'assessmentProvider' => [
                'type' => Type::string()
            ],
            'description' => [
                'type' => Type::string()
            ],
            'user' => [
                'type' => GraphQL::type('User')
            ],
            'provider' => [
                'type' => GraphQL::type('Provider')
            ],
            'customer' => [
                'type' => GraphQL::type('Customer')
            ],
            'order' => [
                'type' => GraphQL::type('Order')
            ],

        ];
    }

}