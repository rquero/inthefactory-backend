<?php

namespace App\GraphQL\Type;

//use GraphQL\Type\Definition\Type;
//use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Models\Customer;
use App\Models\User;


class CustomerProfileType extends BaseType
{
    protected $attributes = [
        'name' => 'CustomerProfile',
        'description' => 'A CustomerProfile'
    ];

    public function fields()
    {
        return [
            'name' => [
                'type' => Type::string()
            ],
            'lastName' => [
                'type' => Type::string()
            ],
            'nameCompany' => [
                'type' => Type::string()
            ],
            'juridical' => [
                'type' => Type::boolean()
            ],
            'status' => [
                'type' => Type::string()
            ],
            'imageProfile' => [
                'type' => Type::string()
            ],
            'orderCanceled' => [
                'type' => Type::int()
            ],
            'orderCompleted' => [
                'type' => Type::int()
            ],
            'orderOthers' => [
                'type' => Type::int()
            ],
            'questionPending' => [
                'type' => Type::int()
            ],
            'questionAnswered' => [
                'type' => Type::int()
            ],
            'totalPositive' => [
                'type' => Type::int()
            ],
            'totalNegative' => [
                'type' => Type::int()
            ],
            'totalNeutral' => [
                'type' => Type::int()
            ]
        ];
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->created_at->toDateTimeString();
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updated_at->toDateTimeString();
    }
}
