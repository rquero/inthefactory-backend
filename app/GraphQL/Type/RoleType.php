<?php

namespace App\GraphQL\Type;

//use GraphQL\Type\Definition\Type;
//use Rebing\GraphQL\Support\Type as GraphQLType;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Models\Role;

class RoleType extends BaseType
{
    protected $attributes = [
        'name' => 'Role',
        'description' => 'A Role',
        'model'       => Role::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int())
            ],
            'name' => [
                'type' => Type::nonNull(Type::string())
            ],
            'description' => [
                'type' => Type::nonNull(Type::string())
            ],
            'status' => [
                'type' => Type::nonNull(Type::boolean())
            ],
            'appAccess' => [
                'type' => Type::nonNull(Type::boolean())
            ],
            'createdAt' => [
                'type' => Type::nonNull(Type::string())
            ],
            'updatedAt' => [
                'type' => Type::nonNull(Type::string())
            ],
            'created' => [
                'type' => Type::nonNull(Type::string())
            ],
            'modified' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->createdAt->toDateTimeString();
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updatedAt->toDateTimeString();
    }

    protected function resolveCreatedField($root, $args)
    {
        return (string) $root->created->toDateTimeString();
    }

    protected function resolveModifiedField($root, $args)
    {
        return (string) $root->modified->toDateTimeString();
    }
}
