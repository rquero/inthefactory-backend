<?php

namespace App\GraphQL\Type;

//use GraphQL\Type\Definition\Type;
//use Rebing\GraphQL\Support\Type as GraphQLType;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Models\Answer;
use App\Models\Question;

class AnswerType extends BaseType
{
    protected $attributes = [
        'name' => 'Answer',
        'description' => 'A Answer',
        'model'       => Answer::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int())
            ],
            'description' => [
                'type' => Type::nonNull(Type::string())
            ],
            'status' => [
                'type' => Type::nonNull(Type::boolean())
            ],
            'question' => [
                'type' => GraphQL::type('Question')
            ],
            'createdAt' => [
                'type' => Type::nonNull(Type::string())
            ],
            'updatedAt' => [
                'type' => Type::nonNull(Type::string())
            ],
            'created_at' => [
                'type' => Type::nonNull(Type::string())
            ],
            'updated_at' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->createdAt->toDateTimeString();
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updatedAt->toDateTimeString();
    }

    protected function resolveCreatedField($root, $args)
    {
        return (string) $root->created_at->toDateTimeString();
    }

    protected function resolveModifiedField($root, $args)
    {
        return (string) $root->updated_at->toDateTimeString();
    }
}
