<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Models\Customer;
use App\Models\User;


class ProviderProfileType extends BaseType
{
    protected $attributes = [
        'name' => 'ProviderProfile',
        'description' => 'A ProviderProfile'
    ];

    public function fields()
    {
        return [
            'nameCompany' => [
                'type' => Type::string()
            ],
            'businessType' => [
                'type' => Type::string()
            ],
            'status' => [
                'type' => Type::string()
            ],
            'imageProfile' => [
                'type' => Type::string()
            ],
            'orders' => [
                'type' => Type::int()
            ],
            'products' => [
                'type' => Type::int()
            ],
            'productReady' => [
                'type' => Type::int()
            ],
            'productActive' => [
                'type' => Type::int()
            ],
            'orderCanceled' => [
                'type' => Type::int()
            ],
            'orderCompleted' => [
                'type' => Type::int()
            ],
            'orderOthers' => [
                'type' => Type::int()
            ],
            'questionPending' => [
                'type' => Type::int()
            ],
            'questionAnswered' => [
                'type' => Type::int()
            ],

            'totalFacility' => [
                'type' => Type::int()
            ],

            'totalResponsability' => [
                'type' => Type::int()
            ],

            'totalService' => [
                'type' => Type::int()
            ],

        ];
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->created_at->toDateTimeString();
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updated_at->toDateTimeString();
    }
}
