<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class CountryType extends BaseType
{
    protected $attributes = [
        'name' => 'Country',
        'description' => 'Country type'
    ];

    public function fields()
    {
        return [
        	'id' => [
                'type' => Type::nonNull(Type::int())
            ],
            'value' => [
                'type' => Type::nonNull(Type::string())
            ],
            'name' => [
                'type' => Type::nonNull(Type::string())
            ],
            'status' => [
                'type' => Type::nonNull(Type::string())
            ],
            'locale' => [
                'type' => Type::nonNull(Type::string())
            ],
            'currency_id' => [
                'type' => Type::nonNull(Type::string())
            ],
            'decimal_separator' => [
                'type' => Type::nonNull(Type::string())
            ],
            'thousands_separator' => [
                'type' => Type::nonNull(Type::string())
            ],
            'time_zone' => [
                'type' => Type::nonNull(Type::string())
            ],
            'image' => [
                'type' => Type::nonNull(Type::string())
            ]
            
        ];
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->created_at->toDateTimeString();
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updated_at->toDateTimeString();
    }
}
