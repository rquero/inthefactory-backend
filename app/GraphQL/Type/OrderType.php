<?php
namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;

class OrderType extends BaseType
{
   
    protected $attributes = [
        'name' => 'Order',
        'description' => 'Order type.'
    ];
    
    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int())
            ],
            'state' => [
                'type' => Type::nonNull(Type::string())
            ],
            'description' => [
                'type' => Type::nonNull(Type::string())
            ],
            'status' => [
                'type' => Type::nonNull(Type::boolean())
            ],
            'note' => [
                'type' => Type::nonNull(Type::string())
            ],
            'quantity' => [
                'type' => Type::nonNull(Type::int())
            ],
            'total' => [
                'type' => Type::nonNull(Type::int())
            ],
            'user' => [
                'type' => GraphQL::type('User')
            ],
            'provider' => [
                'type' => GraphQL::type('Provider')
            ],
            'valorations' => [
                'type' => Type::listOf(GraphQL::type('Valoration'))
            ],
            'createdAt' => [
                'type' => Type::string()
            ],
            'updatedAt' => [
                'type' => Type::string()
            ]
        ];
    }

}