<?php

namespace App\GraphQL\Type;

//use GraphQL\Type\Definition\Type;
//use Rebing\GraphQL\Support\Type as GraphQLType;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Models\Question;
use App\Models\Product;
use App\Models\User;
use App\Models\Answer;

class QuestionType extends BaseType
{
    protected $attributes = [
        'name' => 'Question',
        'description' => 'A Question',
        'model'       => Question::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int())
            ],
            'description' => [
                'type' => Type::nonNull(Type::string())
            ],
            'state' => [
                'type' => Type::nonNull(Type::string())
            ],
            'status' => [
                'type' => Type::nonNull(Type::boolean())
            ],
            'canceled' => [
                'type' => Type::nonNull(Type::boolean())
            ],
            'product' => [
                'type' => GraphQL::type('Product')
            ],
            'user' => [
                'type' => GraphQL::type('User')
            ],
            'answers' => [
                'type' => Type::listOf(GraphQL::type('Answer'))
            ],
            'createdAt' => [
                'type' => Type::nonNull(Type::string())
            ],
            'updatedAt' => [
                'type' => Type::nonNull(Type::string())
            ],
            'created_at' => [
                'type' => Type::nonNull(Type::string())
            ],
            'updated_at' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->createdAt->toDateTimeString();
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updatedAt->toDateTimeString();
    }

    protected function resolveCreatedField($root, $args)
    {
        return (string) $root->created_at->toDateTimeString();
    }

    protected function resolveModifiedField($root, $args)
    {
        return (string) $root->updated_at->toDateTimeString();
    }
}
