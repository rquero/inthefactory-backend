<?php

namespace App\GraphQL\Type;

//use GraphQL\Type\Definition\Type;
//use Rebing\GraphQL\Support\Type as GraphQLType;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Models\Address;
use App\Models\Question;

class AddressType extends BaseType
{
    protected $attributes = [
        'name' => 'Address',
        'description' => 'A Address'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::int()
            ],
            'line1' => [
                'type' => Type::string()
            ],
            'line2' => [
                'type' => Type::string()
            ],
            'status' => [
                'type' => Type::boolean()
            ],
            'state' => [
                'type' => GraphQL::type('State')
            ],
            'type' => [
                'type' => Type::string()
            ],
            'zipCode' => [
                'type' => Type::string()
            ],
            'createdAt' => [
                'type' => Type::string()
            ],
            'updatedAt' => [
                'type' => Type::string()
            ],
            'created_at' => [
                'type' => Type::string()
            ],
            'updated_at' => [
                'type' => Type::string()
            ]
        ];
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->createdAt->toDateTimeString();
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updatedAt->toDateTimeString();
    }

    protected function resolveCreatedField($root, $args)
    {
        return (string) $root->created_at->toDateTimeString();
    }

    protected function resolveModifiedField($root, $args)
    {
        return (string) $root->updated_at->toDateTimeString();
    }
}
