<?php

namespace App\GraphQL\Type;

//use GraphQL\Type\Definition\Type;
//use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Models\User;
use App\Models\Address;
use App\Models\UserAddress;


class UserAddressType extends BaseType
{
    protected $attributes = [
        'name' => 'UserAddress',
        'description' => 'A UserAddress',
        'model'       => UserAddress::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int())
            ],
            'user' => [
                'type' => Type::nonNull(GraphQL::type('User'))
            ],
            'address' => [
                'type' => Type::nonNull(GraphQL::type('Address'))
            ]
        ];
    }
}
