<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Models\User;
use App\Models\Role;
use App\Models\State;
use App\Models\Address;

class UserType extends BaseType
{
    protected $attributes = [
        'name'          => 'User',
        'description'   => 'A user',
        'model'         => User::class,
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int())
            ],
            'name' => [
                'type' => Type::string()
            ],
            'lastName' => [
                'type' => Type::string()
            ],
            'phone' => [
                'type' => Type::string()
            ],
            'imageProfile' => [
                'type' => Type::int()
            ],
            'imageBanner' => [
                'type' => Type::int()
            ],
            'juridical' => [
                'type' => Type::boolean()
            ],
            'status' => [
                'type' => Type::boolean()
            ],
            'nif' => [
                'type' => Type::string()
            ],
            'businessPhone' => [
                'type' => Type::string()
            ],
            'foundationYear' => [
                'type' => Type::string()
            ],
            'nameCompany' => [
                'type' => Type::string()
            ],
            'businessType' => [
                'type' => Type::string()
            ],
            'email' => [
                'type' => Type::string()
            ],
            'emailVerified' => [
                'type' => Type::boolean()
            ],
            'appAccess' => [
                'type' => Type::int()
            ],
            'numberCountries' => [
                'type' => Type::string()
            ],
            'webPage' => [
                'type' => Type::string()
            ],
            'facebook' => [
                'type' => Type::string()
            ],
            'twitter' => [
                'type' => Type::string()
            ],
            'instagram' => [
                'type' => Type::string()
            ],
            'google_' => [
                'type' => Type::string()
            ],
            'description' => [
                'type' => Type::string()
            ],
            'shortDescription' => [
                'type' => Type::string()
            ],
            'typeCompany' => [
                'type' => Type::string()
            ],
            'mainActivity' => [
                'type' => Type::string()
            ],
            'directJobs' => [
                'type' => Type::int()
            ],
            'indirectJobs' => [
                'type' => Type::string()
            ],
            'technicalClassification' => [
                'type' => Type::string()
            ],
            'nationalities' => [
                'type' => Type::string()
            ],
            'averageWages' => [
                'type' => Type::string()
            ],
            'academicProfile' => [
                'type' => Type::string()
            ],
            'numberSupportCompanies' => [
                'type' => Type::string()
            ],
            'typeSupportCompanies' => [
                'type' => Type::string()
            ],
            'mainMarkets' => [
                'type' => Type::string()
            ],
            'laborIncentives' => [
                'type' => Type::string()
            ],
            'trainingPrograms' => [
                'type' => Type::string()
            ],
            'socialResponsibilityPrograms' => [
                'type' => Type::string()
            ],
            'environmentalPrograms' => [
                'type' => Type::string()
            ],
            'ruc' => [
                'type' => Type::string()
            ],
            'dv' => [
                'type' => Type::string()
            ],
            'webUserId' => [
                'type' => Type::string()
            ],
            'webUserType' => [
                'type' => Type::string()
            ],
            'address' => [
                'type' => Type::listOf(GraphQL::type('UserAddress')),
            ],
            'createdAt' => [
                'type' => Type::string()
            ],
            'updatedAt' => [
                'type' => Type::string()
            ],
            'created_at' => [
                'type' => Type::string()
            ],
            'updated_at' => [
                'type' => Type::string()
            ]

        ];
    }

    // If you want to resolve the field yourself, you can declare a method
    // with the following format resolve[FIELD_NAME]Field()



   
}