<?php

namespace App\GraphQL\Mutation\Answer;

use GraphQL;
use App\Models\Answer;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use JWTAuth;

class BlockedAnswerMutation extends Mutation {

    protected $attributes = [
        'name' => 'deleteAnswer',
        'description' => 'Blocked a Answer.'

    ];

    public function type()
    {
        return GraphQL::type('Answer');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int())],
            'status' => ['name' => 'status', 'type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $Answer = Answer::find($args['id']);

        if($Answer) {

            $Answer->status = $args['status'];
            $Answer->save();
            return $Answer;
        }


        return null;
    }

}