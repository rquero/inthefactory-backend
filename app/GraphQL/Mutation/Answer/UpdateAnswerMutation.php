<?php

namespace App\GraphQL\Mutation\Answer;

use GraphQL;
use App\Models\Answer;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use JWTAuth;

class UpdateAnswerMutation extends Mutation {

    protected $attributes = [
        'name' => 'updateAnswer',
        'description' => 'Update a Answer.'

    ];

    public function type()
    {
        return GraphQL::type('Answer');
    }

    public function args()
    {
        return [

            'id' => ['type' => Type::nonNull(Type::int())],
            'description' => [ 'type' => Type::string()],
            'question' => ['type' => Type::id() ]
        ];
    }

    public function resolve($root, $args)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $Answer = Answer::find($args['id']);

        if(!$Answer)
        {
            return null;
        }

        // update Answer
        $Answer->update([
            'description' => $args['description'],
            'questionId'     => $args['question'],
        ]);

        return $Answer;

    }

}