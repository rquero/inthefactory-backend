<?php

namespace App\GraphQL\Mutation\Answer;


use GraphQL;
use JWTAuth;
use App\Models\Answer;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;

class CreateAnswerMutation extends Mutation
{
    protected $attributes = [
        'name' => 'CreateAnswer',
        'description' => 'Create a new Answer.'
    ];

    public function type()
    {
        return GraphQL::type('Answer');
    }

    public function args()
    {
        return [
            
            'description' => [
                'type' => Type::nonNull(Type::string()),
            ],
            'question' => [
                'type' => Type::nonNull(Type::id())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $data = [
            'questionId'     => $args['question'],
            'description' => $args['description'],
            'status' => 1
        ];

        $Answer = Answer::create($data);

        return $Answer;

    }
}
