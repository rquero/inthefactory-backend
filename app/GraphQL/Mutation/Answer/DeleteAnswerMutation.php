<?php

namespace App\GraphQL\Mutation\Answer;

use GraphQL;
use App\Models\Answer;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use JWTAuth;

class DeleteAnswerMutation extends Mutation {

    protected $attributes = [
        'name' => 'deleteAnswer',
        'description' => 'Delete a Answer.'

    ];

    public function type()
    {
        return GraphQL::type('Answer');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        if( $Answer = Answer::findOrFail($args['id']) ) {
            $Answer->delete();
            return $Answer;
        }
        return null;
    }

}