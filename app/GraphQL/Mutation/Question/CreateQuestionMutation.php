<?php

namespace App\GraphQL\Mutation\Question;


use GraphQL;
use JWTAuth;
use App\Models\Question;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;

class CreateQuestionMutation extends Mutation
{
    protected $attributes = [
        'name' => 'CreateQuestion',
        'description' => 'Create a new Question.'
    ];

    public function type()
    {
        return GraphQL::type('Question');
    }

    public function args()
    {
        return [
            
            'description' => [
                'type' => Type::nonNull(Type::string()),
            ],
            'user' => [
                'type' => Type::nonNull(Type::id()),
            ],
            'product' => [
                'type' => Type::nonNull(Type::id()),
            ],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/


        // state : 'Pending','Blocked up','Answered','View','Read'

        $data = [
            'userId'        => $args['user'],
            'productId'     => $args['product'],
            'description'   => $args['description'],
            'status'        => 1,
            'canceled'      => false,
            'state'         => 'Pending',
        ];

        $Question = Question::create($data);

        return $Question;

    }
}
