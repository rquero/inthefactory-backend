<?php

namespace App\GraphQL\Mutation\Question;

use GraphQL;
use App\Models\Question;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use JWTAuth;

class StateQuestionMutation extends Mutation {

    protected $attributes = [
        'name' => 'stateQuestion',
        'description' => 'State a Question.'

    ];

    public function type()
    {
        return GraphQL::type('Question');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int())],
            'state' => ['name' => 'state', 'type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $Question = Question::find($args['id']);

        if($Question) {

            $Question->state = $args['state'];
            $Question->save();
            return $Question;
        }


        return null;
    }

}