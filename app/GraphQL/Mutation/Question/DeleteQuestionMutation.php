<?php

namespace App\GraphQL\Mutation\Question;

use GraphQL;
use App\Models\Question;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use JWTAuth;

class DeleteQuestionMutation extends Mutation {

    protected $attributes = [
        'name' => 'deleteQuestion',
        'description' => 'Delete a Question.'

    ];

    public function type()
    {
        return GraphQL::type('Question');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int())]
        ];
    }

    public function resolve($root, $args)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        if( $Question = Question::findOrFail($args['id']) ) {
            $Question->delete();
            return $Question;
        }
        return null;
    }

}