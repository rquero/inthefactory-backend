<?php

namespace App\GraphQL\Mutation\Question;

use GraphQL;
use App\Models\Question;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use JWTAuth;

class UpdateQuestionMutation extends Mutation {

    protected $attributes = [
        'name' => 'updateQuestion',
        'description' => 'Update a Question.'

    ];

    public function type()
    {
        return GraphQL::type('Question');
    }

    public function args()
    {
        return [

            'id' => ['type' => Type::nonNull(Type::int())],
            'product' => [ 'type' => Type::id()],
            'user' => [ 'type' => Type::id()],
            'description' => [ 'type' => Type::string()]

        ];
    }

    public function resolve($root, $args)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $Question = Question::find($args['id']);

        if(!$Question)
        {
            return null;
        }

        // update Question
        $Question->update([
            'userId'        => $args['user'],
            'productId'     => $args['product'],
            'description'   => $args['description']
        ]);

        return $Question;

    }

}