<?php

namespace App\GraphQL\Mutation\User;

use GraphQL;
use App\Models\User;
use App\Models\Address;
use App\Models\UserAddress;

use App\Models\RoleMapping;
use App\Models\Role;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use JWTAuth;
use DB;

class UpdatePreferencesMutation extends Mutation {

    protected $attributes = [
        'name' => 'updatePreferences',
        'description' => 'Update a Preferences'

    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [
            'id' => ['type' => Type::nonNull(Type::int())],
            'name' => ['type' => Type::string()],
            'lastName' => ['type' => Type::string()],
            'phone' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

  

        $user = User::find($args['id']);

        if(! $user)
        {
            return null;
        }

        $user->update([
            'name'        => $args['name'],
            'lastName'    => $args['lastName'],
            'phone'       => $args['phone'],
        ]);
  
        return $user;
    }

}