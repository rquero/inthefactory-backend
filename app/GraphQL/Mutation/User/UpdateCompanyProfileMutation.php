<?php

namespace App\GraphQL\Mutation\User;

use GraphQL;
use App\Models\User;
use App\Models\RoleMapping;
use App\Models\Role;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use JWTAuth;

class UpdateCompanyProfileMutation extends Mutation {

    protected $attributes = [
        'name' => 'updateCompanyProfile',
        'description' => 'Update Company Profile'

    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [
            'id' => ['type' => Type::nonNull(Type::int())],
            'nameCompany' => ['type' => Type::string()],
            'businessType' => ['type' => Type::string()],
            'facebook' => ['type' => Type::string()],
            'twitter' => ['type' => Type::string()],
            'instagram' => ['type' => Type::string()],
            'shortDescription' => ['type' => Type::string()],
            'nif' => ['type' => Type::string()],
            'foundationYear' => ['type' => Type::string()],
        ];
    }

    public function resolve($root, $args)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $user = User::find($args['id']);

        if(! $user)
        {
            return null;
        }

        // update user
        $user->update([

          'nameCompany' => $args['nameCompany'],
          'businessType' => $args['businessType'],
          'facebook' => $args['facebook'],
          'twitter' => $args['twitter'],
          'instagram' => $args['instagram'],
          'shortDescription' => $args['shortDescription'],
          'nif' => $args['nif'],
          'foundationYear' => $args['foundationYear'],
        ]);

        return $user;
    }

}