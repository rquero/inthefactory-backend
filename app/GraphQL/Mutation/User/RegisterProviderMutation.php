<?php

namespace App\GraphQL\Mutation\User;

use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL;
use App\Models\User;
use App\Models\RoleMapping;
use App\Models\Provider;

use App\Models\UserAddress;
use App\Models\Address;
use JWTAuth;

class RegisterProviderMutation extends Mutation
{
    protected $attributes = [
        'name' => 'registerProvider',
        'description' => 'Create a new registerProvider.'
    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [
            'name_company' => [
                'type' => Type::nonNull(Type::string())
            ],
            'state' => [
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required']
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required', 'email', 'unique:user']
            ],
            'password' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required', 'min:6']
            ]
            

        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {


        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $provider = Provider::create();
  

        $data = [
            'name'            => '',
            'lastName'        => '',
            'phone'           => '',
            'juridical'       => 1,
            'email'           => $args['email'],
            'password'        => bcrypt($args['password']),
            'status'          => 1,
            'imageBanner'     => 2,
            'imageProfile'    => 1,
            'facebook'        => '',
            'twitter'         => '',
            'instagram'       => '',
            'google_'         => '',
            'appAccess'       => 0,
            'emailVerified'   => 1,
            'webUserId'       => $provider->id,
            'webUserType'     => 'Provider',
            'nameCompany'     => $args['name_company'],
        ];


        $user = User::create($data);


        $data1 = [
            'line1'         => '',
            'line2'         => '',
            'zipCode'       => '',
            'status'        =>  1,
            'type'         => 'shippingAddress',
            'stateId'       => $args['state'],
        ];

        $data2 = [
            'line1'         => '',
            'line2'         => '',
            'zipCode'       => '',
            'status'        =>  1,
            'type'         => 'billingAddress',
            'stateId'       => $args['state'],
        ];


       

        $address1 = Address::create($data1);
        $address2 = Address::create($data2);


        UserAddress::create([
            'userId'     => $user->id,
            'addressId'  => $address1->id,
        ]);

        UserAddress::create([
            'userId'     => $user->id,
            'addressId'  => $address2->id,
        ]);

        return $user;



    }
}
