<?php

namespace App\GraphQL\Mutation\User;

use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL;
use App\Models\User;
use App\Models\RoleMapping;
use App\Models\Customer;

use App\Models\UserAddress;
use App\Models\Address;


use JWTAuth;

class RegisterCustomerMutation extends Mutation
{
    protected $attributes = [
        'name' => 'registerCustomer',
        'description' => 'Create a new registerCustomer.'
    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [

            'juridical' => [
                'type' => Type::boolean()
            ],
            'state' => [
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required']
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required', 'email', 'unique:user']
            ],
            'password' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required', 'min:6']
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {


        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/


        $customer = Customer::create();
  

        $data = [
            'name'            => '',
            'lastName'        => '',
            'phone'           => '',
            'juridical'       => $args['juridical'],
            'email'           => $args['email'],
            'password'        => bcrypt($args['password']),
            'status'          => 1,
            'imageBanner'     => 2,
            'imageProfile'    => 1,
            'facebook'        => '',
            'twitter'         => '',
            'instagram'       => '',
            'google_'         => '',
            'appAccess'       => 0,
            'emailVerified'   => 1,
            'webUserId'       => $customer->id,
            'webUserType'     => 'Customer',
        ];


        $user = User::create($data);


        $data1 = [
            'line1'         => '',
            'line2'         => '',
            'zipCode'       => '',
            'status'        =>  1,
            'type'         => 'shippingAddress',
            'stateId'       => $args['state'],
        ];

        $data2 = [
            'line1'         => '',
            'line2'         => '',
            'zipCode'       => '',
            'status'        =>  1,
            'type'         => 'billingAddress',
            'stateId'       => $args['state'],
        ];


       

        $address1 = Address::create($data1);
        $address2 = Address::create($data2);


        UserAddress::create([
            'userId'     => $user->id,
            'addressId'  => $address1->id,
        ]);

        UserAddress::create([
            'userId'     => $user->id,
            'addressId'  => $address2->id,
        ]);


        return $user;

    }
}
