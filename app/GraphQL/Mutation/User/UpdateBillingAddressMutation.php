<?php

namespace App\GraphQL\Mutation\User;

use GraphQL;
use App\Models\User;
use App\Models\Address;
use App\Models\UserAddress;

use App\Models\RoleMapping;
use App\Models\Role;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use JWTAuth;
use DB;

class UpdateBillingAddressMutation extends Mutation {

    protected $attributes = [
        'name' => 'updateBillingAddress',
        'description' => 'Update a BillingAddress of the user'

    ];

    public function type()
    {
        return GraphQL::type('Address');
    }

    public function args()
    {
        return [
            'id' => ['type' => Type::nonNull(Type::int())],
            'line1' => ['type' => Type::string()],
            'line2' => ['type' => Type::string()],
            'state' => ['type' => Type::int()],
        ];
    }

    public function resolve($root, $args)
    {

        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

  

        $UserAddress = UserAddress::where('userId',$args['id'])->get();

        if(! $UserAddress)
        {
            return null;
        }


        foreach ($UserAddress as $key => $value) {

            $Address = Address::where('id',$value->addressId)->first();
            if($Address->type === 'billingAddress')
            {
                $Address->update([
                    'line1'    => $args['line1'],
                    'line2'    => $args['line2'],
                    'stateId'  => $args['state'],
                ]);
            }
           

        }

        return $Address;
    }

}