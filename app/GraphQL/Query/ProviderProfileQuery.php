<?php
namespace App\GraphQL\Query;

use GraphQL;
use JWTAuth;
use App\Models\User;
use App\Models\Order;
use App\Models\Question;
use App\Models\Product;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

class ProviderProfileQuery extends Query {

    protected $attributes = [
        'name' => 'providerProfile',
        'description' => 'Provider Profile.'
    ];

    public function type()
    {
        return GraphQL::type('ProviderProfile');
    }

    public function args()
    {
        return [
            'userId' => [
                'name' => 'userId',
                'type' => Type::nonNull(Type::int())
            ]
        ];
    }

    public function resolve($root, $args)
    {
        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        
        $user = User::find($args['userId']);

        $provider = $user->webUserId;
        $type     = $user->webUserType;
        $image    = $user->imageProfile;
        


        if( $type === 'Provider') {
           
        

                $orders = Order::where('providerId',$provider)->where('state','Valued')->get();  
                $products = Product::where('providerId',$provider)->count();  
                
                $productActive = Product::where('providerId',$provider)->where('state','Active')->count();  
                $productReady = Product::where('providerId',$provider)->where('state','Ready')->count();  

                $orderTT = Order::where('providerId',$provider)->get();
                $orderT = Order::where('providerId',$provider)->count();


                $canceled = 0;
                $completed = 0;
                $others = 0;

                foreach ($orderTT as $key) {
                    if ($key->state === 'Canceled') {

                      $canceled = $canceled + 1 ;
                        
                    }else{

                        if ($key->state === 'Completed') {

                           $completed = $completed + 1 ;

                        }else{
                           $others ++;
                        }

                    }

                }


            
                $questionP = Question::where('userId',$args['userId'])->where('state','Pending')->count();
                $questionA = Question::where('userId',$args['userId'])->where('state','Answered')->count();

                $totalFacility = 0;
                $totalResponsability = 0;
                $totalService  = 0;

               


                foreach ($orders as $key) {

                    foreach ($key->valorations as $keyy) {


                        if ($keyy->user->webUserType === "Customer") {
                            
                        
                           
                                $totalFacility = $totalFacility + $keyy->facility; 
                    

                     
                                $totalResponsability = $totalResponsability + $keyy->human;
                      
                  
                                $totalService = $totalService + $keyy->service;
                           


                        }
                    }


                }



                $details = array(
                    'nameCompany'          => $user['nameCompany'],
                    'businessType'         => $user['businessType'],
                    'status'               => $user['status'],
                    'imageProfile'         => "http://132.148.139.194:3001/api/images/".$image."/download",
                    'orders'               => $orderT,
                    'products'             => $products,
                    'productActive'        => $productActive,
                    'productReady'         => $productReady,
                    'orderCanceled'        => $canceled,     
                    'orderCompleted'       => $completed,
                    'orderOthers'          => $others,
                    'questionPending'      => $questionP,     
                    'questionAnswered'     => $questionA,
                    'totalFacility'        => $totalFacility,      
                    'totalResponsability'  => $totalResponsability,      
                    'totalService'         => $totalService,      
                );

                return $details;
         
        }else{

            return null;

        }





        
    }
}