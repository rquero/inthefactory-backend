<?php
namespace App\GraphQL\Query;

use GraphQL;
use App\Models\Country;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use JWTAuth;

class CountrysQuery extends Query {

    protected $attributes = [
        'name' => 'Countrys',
        'description' => 'Countrys list.'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Country'));
    }

    public function args()
    {
        
    }

    public function resolve($root, $args)
    {
        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/
       
        $countrys = Country::all();
        return $countrys;
    }
}