<?php
namespace App\GraphQL\Query;

use GraphQL;
use App\Models\State;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use JWTAuth;

class StatesQuery extends Query {

    protected $attributes = [
        'name' => 'states',
        'description' => 'States list.'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('State'));
    }

    public function args()
    {
        
    }

    public function resolve($root, $args)
    {
        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/
       
        $states = State::all();
        return $states;
    }
}