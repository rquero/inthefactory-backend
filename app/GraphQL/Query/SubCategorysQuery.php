<?php
namespace App\GraphQL\Query;

use GraphQL;
use App\Models\SubCategory;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use JWTAuth;

class SubCategorysQuery extends Query {

    protected $attributes = [
        'name' => 'subCategorys',
        'description' => 'SubCategorys list.'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('SubCategory'));
    }

    public function args()
    {
        
    }

    public function resolve($root, $args)
    {
        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/
       
        $SubCategory = SubCategory::all();
        return $SubCategory;
    }
}