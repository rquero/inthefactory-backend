<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use App\Models\SubCategory;

class SubCategoryQuery extends Query
{
    protected $attributes = [
        'name' => 'subCategory',
        'description' => 'Find an subCategory.'
    ];

    public function type()
    {
        return GraphQL::type('SubCategory');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        
        return SubCategory::find($args['id']);

    }
}
