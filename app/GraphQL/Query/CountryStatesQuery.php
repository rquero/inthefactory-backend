<?php
namespace App\GraphQL\Query;

use GraphQL;
use App\Models\State;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use JWTAuth;

class CountryStatesQuery extends Query {

    protected $attributes = [
        'name' => 'Country States',
        'description' => 'Country States list.'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('State'));
    }

    public function args()
    {
        return [
            'country' => [
                'name' => 'country',
                'type' => Type::nonNull(Type::int())
            ]
        ];
    }

    public function resolve($root, $args)
    {
        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/
       
        $states = State::where('countryId',$args['country'])->get();

        //User::where('role_id',$args['role'])->skip($skip)->take(constant('LIMIT'))->get();
        return $states;
    }
}