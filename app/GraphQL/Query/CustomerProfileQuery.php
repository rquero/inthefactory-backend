<?php
namespace App\GraphQL\Query;

use GraphQL;
use JWTAuth;
use App\Models\User;
use App\Models\Order;
use App\Models\Question;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

class CustomerProfileQuery extends Query {

    protected $attributes = [
        'name' => 'customerProfile',
        'description' => 'Customer Profile.'
    ];

    public function type()
    {
        return GraphQL::type('CustomerProfile');
    }

    public function args()
    {
        return [
            'userId' => [
                'name' => 'userId',
                'type' => Type::nonNull(Type::int())
            ]
        ];
    }

    public function resolve($root, $args)
    {
        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/


        $user = User::find($args['userId']);


        $customer = $user->webUserId;
        $type     = $user->webUserType;
        $image    = $user->imageProfile;
        



        if($type === 'Customer' ) {
          

                $orders = Order::where('userId',$args['userId'])->where('state','Valued')->get();  

                $orderTT = Order::where('userId',$args['userId'])->get();


                $canceled = 0;
                $completed = 0;
                $others = 0;

                foreach ($orderTT as $key) {
                    if ($key->state === 'Canceled') {

                      $canceled = $canceled + 1 ;
                        
                    }else{

                        if ($key->state === 'Completed') {

                           $completed = $completed + 1 ;

                        }else{
                           $others ++;
                        }

                    }

                }


            
                $questionP = Question::where('userId',$args['userId'])->where('state','Pending')->count();
                $questionA = Question::where('userId',$args['userId'])->where('state','Answered')->count();

                $totalPositive = 0;
                $totalNegative = 0;
                $totalNeutral  = 0;

               


                foreach ($orders as $key) {

                    foreach ($key->valorations as $keyy) {

                            
                            if ($keyy->assessmentProvider === "positive") {
                                $totalPositive = $totalPositive + 1; 
                            }

                            if ($keyy->assessmentProvider === "negative") {
                                $totalNegative = $totalNegative + 1;
                            }

                            if ($keyy->assessmentProvider === "neutral") {
                                $totalNeutral = $totalNeutral + 1;
                            }

                    }


                }



                $details = array(
                    'name'             => $user['name'],
                    'lastName'         => $user['lastName'],
                    'nameCompany'      => $user['nameCompany'],
                    'juridical'        => $user['juridical'],
                    'status'           => $user['status'],
                    'imageProfile'     => "http://132.148.139.194:3001/api/images/".$image."/download",
                    'orderCanceled'    => $canceled,     
                    'orderCompleted'   => $completed,
                    'orderOthers'      => $others,
                    'questionPending'  => $questionP,     
                    'questionAnswered' => $questionA,
                    'totalPositive'    => $totalPositive,      
                    'totalNegative'    => $totalNegative,      
                    'totalNeutral'     => $totalNeutral,      
                );

                return $details;

    

        }else{
          return null;
       
        }
    }
}