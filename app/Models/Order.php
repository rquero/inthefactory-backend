<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "order";
    
    protected $fillable = [
        'state','createdAt','updatedAt','status','note','dateCanceled','dateDispatched','quantity','total','dateSent','userId','providerId'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','userId','id');
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Provider','providerId','id');
    }

    public function valorations() {
        
        return $this->hasMany('App\Models\Valoration', 'orderId', 'id');
        
    }

}
