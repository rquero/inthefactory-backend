<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "question";
     
    protected $fillable = [
       'description','state','canceled','status','productId','updatedAt','createdAt','userId','serviceId','updated_at','created_at'
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product','productId','id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','userId','id');
    }

    function answers() {
		return $this->hasMany("App\Models\Answer", "questionId", "id");
	}
    
}
