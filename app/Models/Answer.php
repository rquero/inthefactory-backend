<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "answer";
     
    protected $fillable = [
       'description','questionId','status','updatedAt','createdAt','updated_at','created_at'
    ];

    public function question()
    {
        return $this->belongsTo('App\Models\Question','questionId','id');
    }
    
}
