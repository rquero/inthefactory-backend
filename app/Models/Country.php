<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "country";
    
    protected $fillable = ['value','name','status','locale','currency_id','decimal_separator','thousands_separator','time_zone','image'];
}
