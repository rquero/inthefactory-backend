<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "subcategory";
    
    protected $fillable = [
        'name','created_at','updated_at','status','description','categoryId'
    ];

    public function category()
    {
        return $this->belongsTo('App\Models\Category','categoryId','id');
    }

}
