<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Valoration extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "valoration";
    
    protected $fillable = [
        'facility','human','service','assessmentProvider','description','userId','providerId','customerId','orderId'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','userId','id');
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Provider','providerId','id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer','customerId','id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order','orderId','id');
    }
    

}
