<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User  as Authenticatable;

class User extends Authenticatable
{
    use  Notifiable;


    protected $table = "user";

    protected $fillable = [
        'webUserType','webUserId','email','password','name', 'lastName', 'phone', 'imageBanner', 'imageProfile', 'facebook', 'twitter', 'instagram', 'google_', 'nameCompany', 'businessType', 'nif', 'businessPhone', 'foundationYear', 'description', 'juridical', 'status', 'emailVerified', 'appAccess', 'numberCountries', 'webPage', 'shortDescription', 'typeCompany', 'mainActivity', 'directJobs', 'indirectJobs', 'technicalClassification', 'nationalities', 'averageWages', 'academicProfile', 'numberSupportCompanies', 'typeSupportCompanies', 'mainMarkets', 'laborIncentives', 'trainingPrograms', 'socialResponsibilityPrograms', 'environmentalPrograms', 'ruc', 'dv','created_at','updated_at','createdAt','updatedAt',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'int'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    function address() {
        return $this->hasMany("App\Models\UserAddress", "userId", "id");
    }

   /* public function role()
    {

        return $this->belongsTo('App\Models\Role','role_id','id');
    }

    public function state()
    {

        return $this->belongsTo('App\Models\State','state_id','id');
    }*/
}
