<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "useraddress";
    
    protected $fillable = [
        'userId', 'addressId'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','userId','id');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\Address','addressId','id');
    }
    

}
