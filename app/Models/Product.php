<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "product";


    protected $fillable = ['name' , 'description' , 'stock' , 'shippingDescription', 'guarantee' , 'stockLastUpdated' , 'canceled' , 'state' , 'status' , 'package_dimensions' , 'package_weight' , 'package_long' , 'package_wide' , 'package_high' , 'product_dimensions' , 'product_weight' , 'product_long' , 'product_wide' , 'product_high' , 'payment_method' , 'contact_method' , 'shortDescription' , 'createdAt' , 'updatedAt' , 'measurementUnitPackageId' , 'measurementUnitProductId' , 'providerId' , 'imageIds' , 'tagIds' , 'subCategoryId' , 'priceId'];


    public function provider()
    {
        return $this->belongsTo('App\Models\Provider','providerId','id');
    }

    function questions() {
		return $this->hasMany("App\Models\Question", "productId", "id");
	}
    

}
