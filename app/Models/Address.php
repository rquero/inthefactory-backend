<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "address";
    
    protected $fillable = [
        'status', 'line1','line2','zipCode','type','stateId'
    ];

    function useraddress() {
		return $this->hasMany("App\Models\UserAddress", "addressId", "id");
	}

    public function state()
    {
        return $this->belongsTo('App\Models\State','stateId','id');
    }
    

}
