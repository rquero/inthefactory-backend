<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "rol";
     
    protected $fillable = [
       'status','appAccess','name','description','createdAt','updatedAt','created','modified'
    ];
    
}
